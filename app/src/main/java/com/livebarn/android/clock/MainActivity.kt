package com.livebarn.android.clock

import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.WindowManager
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : FragmentActivity() {

    private var handler: Handler? = null

    private lateinit var textViewUTC: TextView
    private lateinit var textViewMontreal: TextView
    private lateinit var textViewLee: TextView
    private lateinit var textViewZhengping: TextView
    private lateinit var textViewIndia: TextView
    private lateinit var textViewMilliseconds: TextView
    private lateinit var textViewTodayDate: TextView

    private val timeFormat = "HH:mmZ"
    private val dateFormat = "MMMM dd"
    private val simpleDateTimeFormatUTC = SimpleDateFormat(timeFormat, Locale.getDefault())
    private val simpleDateTimeFormatMontreal = SimpleDateFormat(timeFormat, Locale.getDefault())
    private val simpleDateTimeFormatLee = SimpleDateFormat(timeFormat, Locale.getDefault())
    private val simpleDateTimeFormatZhengping = SimpleDateFormat(timeFormat, Locale.getDefault())
    private val simpleDateTimeFormatIndia = SimpleDateFormat(timeFormat, Locale.getDefault())
    private val simpleDateTimeFormatTodayDate = SimpleDateFormat(dateFormat, Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        textViewUTC = findViewById(R.id.text_view_time_utc)
        textViewMontreal = findViewById(R.id.text_view_time_montreal)
        textViewLee = findViewById(R.id.text_view_time_lee)
        textViewZhengping = findViewById(R.id.text_view_time_zhenping)
        textViewIndia = findViewById(R.id.text_view_time_india)
        textViewMilliseconds = findViewById(R.id.text_view_time_milliseconds)
        textViewTodayDate = findViewById(R.id.text_view_today_date)

        simpleDateTimeFormatUTC.timeZone = TimeZone.getTimeZone("Etc/GMT")
        simpleDateTimeFormatMontreal.timeZone = TimeZone.getTimeZone("America/Montreal")
        simpleDateTimeFormatLee.timeZone = TimeZone.getTimeZone("America/Halifax")
        simpleDateTimeFormatZhengping.timeZone = TimeZone.getTimeZone("America/Edmonton")
        simpleDateTimeFormatIndia.timeZone = TimeZone.getTimeZone("Asia/Kolkata")
    }

    override fun onStart() {
        super.onStart()
        handler = Handler(Looper.getMainLooper())

        displayTimes()
    }

    override fun onStop() {
        super.onStop()
        handler?.removeCallbacks(runnable200)
        handler = null
    }

    private val runnable200 = Runnable {
        displayTimes()
    }

    private fun displayTimes() {
        val date = Calendar.getInstance().time
        textViewUTC.text = getSpannableTime(simpleDateTimeFormatUTC.format(date))
        textViewMontreal.text = getSpannableTime(simpleDateTimeFormatMontreal.format(date))
        textViewLee.text = getSpannableTime(simpleDateTimeFormatLee.format(date))
        textViewZhengping.text = getSpannableTime(simpleDateTimeFormatZhengping.format(date))
        textViewIndia.text = getSpannableTime(simpleDateTimeFormatIndia.format(date))
        textViewMilliseconds.text = "${date.time}"
        textViewTodayDate.text = simpleDateTimeFormatTodayDate.format(date)

        handler?.postDelayed(runnable200, 200L)
    }

    private fun getSpannableTime(time: String?): CharSequence {
        time?.let {
            val spannableStringBuilder = SpannableStringBuilder()
            val spannableStringTime = SpannableString(it.substring(0, 5))
            val spannableString = SpannableString(
                "${it.substring(5, 8)}:${it.substring(8, it.length)}"
            )
            spannableStringTime.setSpan(
                StyleSpan(Typeface.BOLD), 0, spannableStringTime.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spannableString.setSpan(
                RelativeSizeSpan(0.7f), 0, spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spannableStringBuilder.append(spannableStringTime)
            spannableStringBuilder.append(spannableString)
            return spannableStringBuilder
        }

        return ""
    }
}